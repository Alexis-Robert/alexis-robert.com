# alexis-robert.com

## Technologies

- [Next.js](https://www.netlify.com/)
- [Tailwind CSS](https://tailwindcss.com/)
- [GSAP](https://greensock.com/gsap/)
- [Umami](https://umami.is/)