import React from "react";

type LogoProps = {
  className?: string;
  style?: React.CSSProperties;
};

const Logo: React.FC<LogoProps> = ({ ...rest }) => {
  return (
    <div>
      <svg
        {...rest}
        width="48"
        height="35"
        viewBox="0 0 48 35"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M26.8138 0H21.5172L0 35H15.5586L28.8 12.7273L40.3862 30.8636H35.7517L28.8 19.7273L26.1517 23.8636L33.1034 35H48L28.8 3.81818L12.5793 30.8636H8.27586L26.8138 0Z"
          fill="url(#paint0_linear_116_755)"
        />
        <defs>
          <linearGradient
            id="paint0_linear_116_755"
            x1="22.92"
            y1="35"
            x2="22.92"
            y2="1.23924e-06"
            gradientUnits="userSpaceOnUse"
          >
            <stop stopColor="#2563EB" />
            <stop offset="1" stopColor="#25BCDD" />
          </linearGradient>
        </defs>
      </svg>
      <span className="sr-only">Logo représentant la lettre A</span>
    </div>
  );
};

export default Logo;
