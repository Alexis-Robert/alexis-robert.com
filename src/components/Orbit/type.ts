import { ReactNode } from "react";

export type Satelitte = {
  id: string;
  node: ReactNode;
};
