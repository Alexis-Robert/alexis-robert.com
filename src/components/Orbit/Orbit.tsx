import OrbitClient, { OrbitClientProps } from "./OrbitClient";
import OrbitServer from "./OrbitServer";

type OrbitProps = {} & OrbitClientProps;

const Orbit: React.FC<OrbitProps> = ({ satellites, ...rest }) => {
  return (
    <>
      <OrbitClient satellites={satellites} {...rest} />
      <OrbitServer satellites={satellites} />
    </>
  );
};

export default Orbit;
