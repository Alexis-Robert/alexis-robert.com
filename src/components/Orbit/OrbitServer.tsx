import { Satelitte } from "./type";

type OrbitServerProps = {
  satellites: Satelitte[];
};

const OrbitServer: React.FC<OrbitServerProps> = ({ satellites }) => {
  // Render

  const renderSatellites = (satellites?: Satelitte[]) => {
    return satellites?.map((satellite) => {
      return (
        <li key={satellite.id} className="sr-only">
          {satellite.node}
        </li>
      );
    });
  };

  return <ul>{renderSatellites(satellites)}</ul>;
};

export default OrbitServer;
