import { Github, Gitlab, Linkedin } from "lucide-react";
import React from "react";

type IconProps = {
  name: "github" | "linkedin" | "gitlab";
};

type IconType = {
  [K in IconProps["name"]]: {
    icon: JSX.Element;
    alt: string;
  };
};

const Icon: React.FC<IconProps> = ({ name }) => {
  const icon: IconType = {
    github: {
      icon: <Github />,
      alt: "Github",
    },
    linkedin: {
      icon: <Linkedin />,
      alt: "LinkedIn",
    },
    gitlab: {
      icon: <Gitlab />,
      alt: "Gitlab",
    },
  };

  return (
    <>
      {icon[name].icon}
      <span className="sr-only">{icon[name].alt}</span>
    </>
  );
};

export default Icon;
