import Image from "next/image";
import Icon from "./Icon";
import Logo from "./Logo";
import Orbit from "./Orbit";
import Technology from "./Technology";

const Header = () => {
  return (
    <header className="h-screen flex flex-col items-center justify-center">
      <div className="space-y-4 flex flex-col items-center relative justify-center">
        <div>
          <Image
            priority={true}
            className="h-28 w-28 rounded-full object-cover"
            src="/me.jpg"
            height={100}
            width={100}
            alt="Photo de Alexis Robert"
          />
        </div>
        <div className="text-center">
          <div className="flex items-baseline space-x-[2px]">
            <Logo className="h-full w-10 " />
            <h2 className="text-2xl font-bold">
              <span className="sr-only">A</span>lexis Robert
            </h2>
          </div>
          <h1>Développeur Full Stack</h1>
        </div>
        <ul className="flex space-x-4">
          <li>
            <a href="https://github.com/ByKyZo" target="_blank">
              <Icon name="github" />
            </a>
          </li>
          <li>
            <a href="https://gitlab.com/Alexis-Robert" target="_blank">
              <Icon name="gitlab" />
            </a>
          </li>
          <li>
            <a
              href="https://www.linkedin.com/in/alexis-robert-b594301bb/"
              target="_blank"
            >
              <Icon name="linkedin" />
            </a>
          </li>
          <li>
            <a
              className="text-xl font-semibold"
              href="https://www.linkedin.com/in/alexis-robert-b594301bb/"
              target="_blank"
            >
              CV
            </a>
          </li>
        </ul>
      </div>
      <Orbit
        className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 h-[400px] w-[400px]"
        satellites={[
          { node: <Technology name="reactjs" />, id: "react" },
          { node: <Technology name="nextjs" />, id: "nextjs" },
          { node: <Technology name="nodejs" />, id: "nodejs" },
          { node: <Technology name="tailwindcss" />, id: "tailwindcss" },
          { node: <Technology name="typescript" />, id: "typescript" },
        ]}
        config={{
          satellitesLoopDuration: 15000,
        }}
      />
    </header>
  );
};

export default Header;
