import Header from "../components/Header";

import type { Metadata } from "next";

export const metadata: Metadata = {
  title: "Alexis Robert - Développeur Full Stack",
  description: "Développement d'applications web et mobiles",
};

export default function Home() {
  return (
    <main className="relative px-24">
      <Header />
    </main>
  );
}
