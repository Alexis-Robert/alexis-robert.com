import { Inter } from "next/font/google";
import Script from "next/script";
import "./globals.css";

const inter = Inter({ subsets: ["latin"] });

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={inter.className}>{children}</body>
      <Script
        defer
        src="https://analytics.eu.umami.is/script.js"
        data-website-id="a0f81ea4-c922-4f5b-bf93-4385080c3ea9"
      />
    </html>
  );
}
