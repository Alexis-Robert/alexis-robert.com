export type Technologies =
  | "reactjs"
  | "tailwindcss"
  | "typescript"
  | "nextjs"
  | "nodejs";
